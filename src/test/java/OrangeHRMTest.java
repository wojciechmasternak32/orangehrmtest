import org.example.OrangeHRMLoginPage;
import org.example.OrangeHRMTimePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.apache.log4j.Logger;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OrangeHRMTest {
    private WebDriver driver;
    private WebDriverWait wait;
    private OrangeHRMLoginPage loginPage;
    private OrangeHRMTimePage timePage;
    private static final Logger logger = Logger.getLogger(OrangeHRMTest.class);

    @BeforeClass
public void setUp() {
    System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");

    // Configure Chrome to run in headless mode
    ChromeOptions options = new ChromeOptions();
    //options.addArguments("--headless");
    options.addArguments("--disable-gpu");
    options.addArguments("--no-sandbox");
    options.addArguments("--disable-dev-shm-usage");
    
    driver = new ChromeDriver(options);
    driver.manage().window().maximize();
    wait = new WebDriverWait(driver, 30);
    loginPage = new OrangeHRMLoginPage(driver, wait);
    timePage = new OrangeHRMTimePage(driver, wait);
    logger.info("setUp done");
}


    @Test
    public void testTimeSheetComment() {
        // Login to OrangeHRM
        driver.get("https://opensource-demo.orangehrmlive.com/");
        loginPage.login("Admin", "admin123");
        logger.info("Logged in");

        // Navigate to Time tab
        timePage.goToTimeTab();
        logger.info("TimeTab reached");

        // Click view button for timesheet period
        timePage.viewTimesheet();
        logger.info("View button clicked");

        // Click green comment button
        timePage.clickCommentButton();
        logger.info("Green comment button clicked");

        // Verify comment text
        String commentText = timePage.getCommentText();
        Assert.assertEquals(commentText, "Leadership Development");
        logger.info("Expected comment text: 'Leadership Development', actual comment text: '" + commentText + "'");


        // Negative test: Verify comment text is not "Incorrect Text"
        Assert.assertNotEquals(commentText, "Incorrect Text");
        logger.info("Expected comment text not to be: 'Incorrect Text', actual comment text: '" + commentText + "'");

    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
