package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrangeHRMTimePage {
    private WebDriver driver;
    private WebDriverWait wait;

    // Locators
    private By timeTabLocator = By.xpath("//span[normalize-space()='Time']");
    private By commentButtonLocator = By.cssSelector(".bi-chat-dots-fill");

    String dynamicPart = "v-e23d7a1e";
    String attributeName = "data-" + dynamicPart;
    String attributeValue;

    By commentTextLocator = By.cssSelector("textarea.oxd-textarea[data-" + dynamicPart + "]");

    String targetDateRange = "2022-08-15 - 2022-08-21";

    public OrangeHRMTimePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void goToTimeTab() {
        WebElement timeTab = wait.until(ExpectedConditions.elementToBeClickable(timeTabLocator));
        timeTab.click();
    }

    public void viewTimesheet() {
        WebElement viewButton = clickViewButton();
        if (viewButton != null) {
            System.out.println("Found the target text: " + targetDateRange);
        } else {
            System.out.println("The element with the specified text was not found: " + targetDateRange);
        }
    }

    public void clickCommentButton() {
        WebElement commentButton = wait.until(ExpectedConditions.elementToBeClickable(commentButtonLocator));
        commentButton.click();
    }

    public String getCommentText() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        By textareaLocator = By.cssSelector(".oxd-textarea.oxd-textarea--active");
        WebElement textareaElement = wait.until(ExpectedConditions.visibilityOfElementLocated(textareaLocator));

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        JavascriptExecutor js = (JavascriptExecutor) driver;
        String commentText = (String) js.executeScript("return arguments[0].value;", textareaElement);
        return commentText;
    }

    private WebElement findTargetElement() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='" + targetDateRange + "']")));
    }

    private WebElement clickViewButton() {
        WebElement targetElement = findTargetElement();
        if (targetElement != null) {
            WebElement viewButton = targetElement.findElement(By.xpath("./ancestor::div[contains(@class, 'oxd-table-card')]//button[contains(@class, 'oxd-button')]"));
            viewButton.click();
            return viewButton;
        } else {
            return null;
        }
    }
}
