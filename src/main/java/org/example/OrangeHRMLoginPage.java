package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrangeHRMLoginPage {
    private WebDriver driver;
    private WebDriverWait wait;

    // Locators
    private By usernameLocator = By.xpath("//input[@placeholder='Username']");
    private By passwordLocator = By.xpath("//input[@placeholder='Password']");
    private By loginButtonLocator = By.xpath("//button[@type='submit']");

    public OrangeHRMLoginPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void login(String username, String password) {
        WebElement usernameInput = wait.until(ExpectedConditions.elementToBeClickable(usernameLocator));
        WebElement passwordInput = wait.until(ExpectedConditions.elementToBeClickable(passwordLocator));
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(loginButtonLocator));

        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
        loginButton.click();
    }
}
